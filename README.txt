Panels REST Expose
==========
Panels REST Expose module contains a REST resource which exposes the panels layout in JSON format.

A Database Query has been fired to get the Panel's metadata by using a URL parameter in REST call and a specific format for the exposed JSON has been suggested.

This module is basically used for development purposes, and should be part of production only after a detailed analysis.

Author
========
Divesh Kumar
